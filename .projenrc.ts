import { PreactModuleProject } from '@tui-boilerplate/mbp-preact-module';
import { KernelProject } from '@tui-boilerplate/modular-boilerplate';
import { javascript } from 'projen';
const project = new KernelProject({
  defaultReleaseBranch: 'main',
  devDeps: ['@tui-boilerplate/modular-boilerplate', '@tui-boilerplate/mixins@^0.2.3', '@tui-boilerplate/mbp-preact-module'],
  name: 'mfe-test',
  packageManager: javascript.NodePackageManager.NPM,
  projenrcTs: true,
  peerDeps: ['@tui-boilerplate/mixins@>=0.2.3 <1.0.0'],

  // deps: [],                /* Runtime dependencies of this module. */
  // description: undefined,  /* The description is just a string that helps people understand the purpose of the package. */
  // packageName: undefined,  /* The "name" in package.json. */
});

new PreactModuleProject({
  defaultReleaseBranch: 'main',
  devDeps: [],
  name: 'frontend',
  packageManager: javascript.NodePackageManager.NPM,
  projenrcTs: true,
  parent: project,
  outdir: 'frontend',
  locales: { EN: 'en-GB' },
  sampleCode: true,
  eslint: false,
});

project.addTask('dev', {
  steps: [
    { exec: 'npm run dev -w frontend' },
  ],
});

project.synth();