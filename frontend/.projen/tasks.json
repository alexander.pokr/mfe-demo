{
  "tasks": {
    "build": {
      "name": "build",
      "description": "Full release build",
      "steps": [
        {
          "spawn": "pre-compile"
        },
        {
          "spawn": "compile"
        },
        {
          "spawn": "post-compile"
        },
        {
          "spawn": "test"
        },
        {
          "spawn": "package"
        }
      ]
    },
    "check:manifest": {
      "name": "check:manifest",
      "steps": [
        {
          "exec": "npx mfe-validate --path=src/openmfe/manifest.yml"
        }
      ]
    },
    "check:ts": {
      "name": "check:ts",
      "steps": [
        {
          "exec": "tsc --noEmit -p ./src/tsconfig.json"
        },
        {
          "exec": "tsc --noEmit -p ./test/tsconfig.json"
        }
      ]
    },
    "clean:dist": {
      "name": "clean:dist",
      "steps": [
        {
          "exec": "del dist/app"
        },
        {
          "exec": "del dist/prerender"
        }
      ]
    },
    "compile": {
      "name": "compile",
      "description": "Only compile",
      "steps": [
        {
          "exec": "tsc --build"
        }
      ]
    },
    "default": {
      "name": "default",
      "description": "Synthesize project files",
      "steps": [
        {
          "exec": "npx projen default",
          "cwd": ".."
        }
      ]
    },
    "dev": {
      "name": "dev",
      "steps": [
        {
          "say": "That command start app in watch mode"
        },
        {
          "exec": "dotenv -e .env concurrently \"npm:dev:*(!mbp)\""
        },
        {
          "spawn": "clean:dist"
        }
      ]
    },
    "dev:app": {
      "name": "dev:app",
      "steps": [
        {
          "exec": "rollup -c -w --environment INCLUDE_DEPS,BUILD:local"
        }
      ]
    },
    "dev:clean-locale": {
      "name": "dev:clean-locale",
      "steps": [
        {
          "exec": "lingui extract --clean"
        }
      ]
    },
    "dev:compile-locale": {
      "name": "dev:compile-locale",
      "steps": [
        {
          "exec": "lingui compile --watch"
        }
      ]
    },
    "dev:extract-locale": {
      "name": "dev:extract-locale",
      "steps": [
        {
          "exec": "lingui extract --watch --clean"
        }
      ]
    },
    "dev:mbp:build": {
      "name": "dev:mbp:build",
      "steps": [
        {
          "say": "That command going to build all frontend using your .env file"
        },
        {
          "spawn": "clean:dist"
        },
        {
          "spawn": "dev:mbp:build:app"
        },
        {
          "spawn": "dev:mbp:build:prerender"
        },
        {
          "spawn": "mbp:build:scss"
        }
      ]
    },
    "dev:mbp:build:app": {
      "name": "dev:mbp:build:app",
      "steps": [
        {
          "exec": "dotenv -e .env npm run mbp:build:app"
        }
      ]
    },
    "dev:mbp:build:compile-locale": {
      "name": "dev:mbp:build:compile-locale",
      "steps": [
        {
          "exec": "lingui compile"
        }
      ]
    },
    "dev:mbp:build:extract-locale": {
      "name": "dev:mbp:build:extract-locale",
      "steps": [
        {
          "exec": "lingui extract --clean"
        }
      ]
    },
    "dev:mbp:build:prerender": {
      "name": "dev:mbp:build:prerender",
      "steps": [
        {
          "exec": "dotenv -e .env npm run mbp:build:prerender --environment INCLUDE_DEPS,BUILD:local"
        }
      ]
    },
    "dev:playground": {
      "name": "dev:playground",
      "steps": [
        {
          "exec": "dotenv npm run playground"
        }
      ]
    },
    "dev:prerender": {
      "name": "dev:prerender",
      "steps": [
        {
          "exec": "rollup --config rollup.prerender.config.mjs -w  --environment INCLUDE_DEPS,BUILD:local"
        }
      ]
    },
    "dev:scss": {
      "name": "dev:scss",
      "steps": [
        {
          "exec": "sass --watch --load-path=../node_modules/ --load-path=./node_modules/ src/styles:dist/app"
        }
      ]
    },
    "eslint": {
      "name": "eslint",
      "steps": [
        {
          "exec": "eslint . --ext .ts --ext .js --ext .cjs --ext .mjs --ext .tsx --fix"
        }
      ]
    },
    "install": {
      "name": "install",
      "description": "Install project dependencies and update lockfile (non-frozen)",
      "steps": [
        {
          "exec": "npm install"
        }
      ]
    },
    "install:ci": {
      "name": "install:ci",
      "description": "Install project dependencies using frozen lockfile",
      "steps": [
        {
          "exec": "npm ci"
        }
      ]
    },
    "mbp:build": {
      "name": "mbp:build",
      "steps": [
        {
          "say": "That command going to build all frontend"
        },
        {
          "spawn": "clean:dist"
        },
        {
          "spawn": "mbp:build:app"
        },
        {
          "spawn": "mbp:build:prerender"
        },
        {
          "spawn": "mbp:build:scss"
        }
      ]
    },
    "mbp:build:app": {
      "name": "mbp:build:app",
      "steps": [
        {
          "exec": "rollup -c rollup.config.mjs"
        }
      ]
    },
    "mbp:build:prerender": {
      "name": "mbp:build:prerender",
      "steps": [
        {
          "exec": "rollup -c rollup.prerender.config.mjs"
        }
      ]
    },
    "mbp:build:scss": {
      "name": "mbp:build:scss",
      "steps": [
        {
          "exec": "sass --style=compressed --load-path=../node_modules/ --load-path=./node_modules/ src/styles:dist/app"
        }
      ]
    },
    "package": {
      "name": "package",
      "description": "Creates the distribution package",
      "steps": [
        {
          "exec": "mkdir -p dist/js"
        },
        {
          "exec": "mv $(npm pack) dist/js/"
        }
      ]
    },
    "playground": {
      "name": "playground",
      "steps": [
        {
          "exec": "npx @tui-mwa/mfe-playground --path dist/app --manifestPath dist/app/openmfe/manifest.yml"
        }
      ]
    },
    "post-compile": {
      "name": "post-compile",
      "description": "Runs after successful compilation"
    },
    "post-upgrade": {
      "name": "post-upgrade",
      "description": "Runs after upgrading dependencies"
    },
    "pre-compile": {
      "name": "pre-compile",
      "description": "Prepare the project for compilation"
    },
    "test": {
      "name": "test",
      "description": "Run tests",
      "steps": [
        {
          "exec": "jest --passWithNoTests --updateSnapshot",
          "receiveArgs": true
        }
      ]
    },
    "test:watch": {
      "name": "test:watch",
      "description": "Run jest in watch mode",
      "steps": [
        {
          "exec": "jest --watch"
        }
      ]
    },
    "upgrade": {
      "name": "upgrade",
      "description": "upgrade dependencies",
      "env": {
        "CI": "0"
      },
      "steps": [
        {
          "exec": "npx npm-check-updates@16 --upgrade --target=minor --peer --dep=dev,peer,prod,optional --filter=@lingui/cli,@lingui/swc-plugin,@rollup/plugin-alias,@swc/core,@swc/jest,@testing-library/jest-dom,@tui-mwa/mfe-playground,@types/jest,dotenv-cli,eslint,eslint-config-airbnb-typescript,eslint-plugin-import,eslint-plugin-jsx-a11y,eslint-plugin-react,jest,jest-environment-jsdom,projen,rollup-plugin-swc3,ts-jest,typescript,@lingui/core,@lingui/macro"
        },
        {
          "exec": "npm install"
        },
        {
          "exec": "npm update @lingui/cli @lingui/swc-plugin @rollup/plugin-alias @rollup/plugin-commonjs @rollup/plugin-json @rollup/plugin-node-resolve @rollup/plugin-replace @swc/core @swc/jest @testing-library/jest-dom @tui-mwa/mfe-playground @types/jest @types/node concurrently constructs del-cli dotenv-cli eslint eslint-config-airbnb-typescript eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react jest jest-environment-jsdom jest-junit preact-render-to-string projen rollup-plugin-copy rollup-plugin-import-css rollup-plugin-node-polyfills rollup-plugin-scss rollup-plugin-string rollup-plugin-swc3 rollup sass ts-jest typescript @lingui/core @lingui/macro @tui/ui-library preact"
        },
        {
          "exec": "npx projen"
        },
        {
          "spawn": "post-upgrade"
        }
      ]
    },
    "watch": {
      "name": "watch",
      "description": "Watch & compile in the background",
      "steps": [
        {
          "exec": "tsc --build -w"
        }
      ]
    }
  },
  "env": {
    "PATH": "$(npx -c \"node --print process.env.PATH\")"
  },
  "//": "~~ Generated by projen. To modify, edit .projenrc.js and run \"npx projen\"."
}
