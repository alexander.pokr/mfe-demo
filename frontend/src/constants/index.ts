import { LocaleEnum } from '../types/locale';

export const MFE_TAG_NAME = 'tui-mbp-preact';

/**
 * attributes, which we receive as configuration for MFE
 */
export enum WebComponentAttributes {
  locale = 'locale',
  market = 'market',
  theme = 'theme',
}

export const Defaults = {
  locale: LocaleEnum.EN,
};
