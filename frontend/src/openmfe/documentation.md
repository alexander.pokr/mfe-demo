# What is that file?

That file SHOULD explain the functionality of the MFE in
two to four paragraphs in a non-technical way from the
business/customer value perspective.

> That file SHOULD include information for people who will use that MFE

## ❗❗❗Attention ❗❗❗

That file **MUST NOT** include any technical, security,
infrastructure, or development information!!!

### Maximum Attention❗❗❗ That file ONLY for public information ❗❗❗
