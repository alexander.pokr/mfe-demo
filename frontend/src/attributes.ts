import { WebComponentAttributes } from './constants';
import { AppAttributeProps } from './types';
import { LocaleEnum } from './types/locale';
import { MarketEnum } from './types/market';
import { ThemeEnum } from './types/theme';

export const prepareAttribute = (propName: keyof AppAttributeProps, value: string | null): Partial<AppAttributeProps> => {
  switch (propName) {
    case 'locale':
      return { locale: value as LocaleEnum || LocaleEnum.EN };
    case 'market':
      return { market:  value as MarketEnum };
    case 'theme':
      return { theme: value as ThemeEnum };
  }
};

export const getAttributes = <T extends HTMLElement>(context: T): AppAttributeProps => {
  return {
    locale: context.getAttribute(WebComponentAttributes.locale) as LocaleEnum || LocaleEnum.EN,
    market: context.getAttribute(WebComponentAttributes.market) as MarketEnum,
    theme: context.getAttribute(WebComponentAttributes.theme) as ThemeEnum,
  };
};