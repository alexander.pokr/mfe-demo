import { Fragment, FunctionComponent } from 'preact';
import { useState } from 'preact/hooks';
import { plural } from '@lingui/macro';

export const Counter: FunctionComponent = () => {
  const [counter, setCounter] = useState<number>(1);

  const increaseCounter = () => setCounter((prev) => prev + 1);
  const decreaseCounter = () =>
    setCounter((prev) => (prev > 0 ? prev - 1 : prev));

  return (
    <Fragment>
      <h5 data-testid="counter">{
        plural(counter, {
          one: '# item',
          other: '# items',
        })}<br/></h5>
      <div className="buttons-wrapper">
        <button
          className="my-btn my-btn--secondary my-btn--medium mfe-btn"
          type="button"
          onClick={decreaseCounter}
        >
          -
        </button>
        <button
          className="my-btn my-btn--secondary my-btn--medium mfe-btn"
          type="button"
          onClick={increaseCounter}
        >
          +
        </button>
      </div>
    </Fragment>
  );
};
