import { FunctionComponent } from 'preact';
import { t } from '@lingui/macro';
import { i18n } from '@lingui/core';
import { Counter } from '../Counter';
import { MarketEnum } from '../../types/market';
import { ThemeEnum } from '../../types/theme';

type ContainerProps = {
  market: MarketEnum;
  theme: ThemeEnum;
};

export const Container: FunctionComponent<ContainerProps> = ({
  market,
  theme,
}) => {

  return (
    <div className={`container theme-${theme}`}>
      <h2 className="header">
        {t`Market is:`}  <span className="header--props">{market}</span>
        <br/>
        <span>{t`Locale is:`} </span>
        <span className="header--props">{i18n.locale}</span>.
      </h2>
      <Counter />
    </div>
  );
};
