import { FunctionComponent } from 'preact';
import { i18n } from '@lingui/core';
import { Container } from './components/Container';
import { AppAttributeProps } from './types';
import { loadLocales } from './locales/locale';

loadLocales(i18n);

export const App: FunctionComponent<AppAttributeProps> = ({
  locale,
  market,
  theme,
}) => {

  i18n.activate(locale as string);

  return (
    <Container market={market} theme={theme} />
  );
};
