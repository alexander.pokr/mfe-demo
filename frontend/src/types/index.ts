import { LocaleEnum } from './locale';
import { MarketEnum } from './market';
import { ThemeEnum } from './theme';

export interface AppAttributeProps {
  locale: LocaleEnum;
  market: MarketEnum;
  theme: ThemeEnum;
}
